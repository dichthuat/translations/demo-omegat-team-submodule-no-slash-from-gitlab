Burden of disease from rising coal  emissions in Asia  International Symposium on Climate Change and Coal   May 29, 2015  

Shannon Koplitz1, Daniel Jacob1 , Lauri Myllyvirta2, Melissa Sulprizio1  1Harvard University  2Greenpeace International  



Coal emissions are harmful to human health  Fine particulate matter (PM2.5)  SO2  NOx  Ozone (O3) Respiratory and  Image sources: cliparts.co; www.envpl.ipb.ac.rs; Jupiterimages cardiovascular disease  Corporation; www.intechopen.com/source/html/42164/media/ image4.png    SO2 and NOx from power plants oxidize in the atmosphere to form particulate  matter (PM). NOx can also increase ozone concentrations. Both PM and  ozone lead to premature mortality in people.   Coal emissions declining in the U.S. due to public health concern  Difference in SO2 emissions (2010 – 2005)  Klimont et al., 2013  Coal emissions in many Asian countries are currently following the same  upwards trajectory that has taken decades to reverse in the U.S. and  Europe.   Coal use is expanding rapidly in Southeast Asia  Coal power plant locations by 2030  Operating  Projected  Sources: Platts WEPP Database, Coalswarm.org  There are currently more than 400 coal plants scheduled for development in  Asia outside China and India. Many of these plants are already under  construction.  Project Objectives  1.  Calculate surface PM and ozone concentrations due to both present day  and estimated 2030 coal emissions in East and Southeast Asia  (excluding emissions from China and India).  

2.  Estimate the human health burden of this rising coal pollution.  Approach  1.  Attribute changes in PM and ozone concentrations due to both present day  (2011) and projected 2030 coal emissions using GEOS-Chem  

2.  Apply concentration-response relationships from the literature (Krewski et  al., 2009; Anenberg et al., 2010) to estimate the premature mortality due to  coal-related pollution.  

Power plant emissions vary widely by facility  Boiler type Emission control technologies  Fluidized Bed  Combustion  (FBC)  Selective catalytic  reduction (NOx)  Flue gas desulferization (SO2)  Stoker Type of coal  Images: Ciris Energy; AECOM Process Technologies; dieselnet.com; energy-models.com/boilers   Plant specific factors such as the grade of coal being combusted or the  emission control technologies in place affect the magnitude and type of  emissions coming from each individual coal fired power plant.  We develop a detailed inventory of the currently operating fleet  S. Korea  Coal SO2 Emissions  (Present Day ~2011)  Japan 7 Myanmar  Vietnam 6 Taiwan 5 Philippines 4 3 Thailand 2 Malaysia 1 0 ChCihninaa InInddiaia U.S.S. .  Countries in  this work  Indonesia Lu et al., 2011; EPA  Annual ARP report 2013  Emissions of SO2 and NOx from coal plants are currently highest in  Indonesia, followed by Thailand and Japan.   Tg yr-1  Coal emissions likely to surpass U.S. levels by 2030  S. Korea  Coal SO2 Emissions  Japan  Myanmar 7 Vietnam  6 Taiwan  5 Increase  Philippines 4 by 2030  3 Thailand 2 Malaysia  1 2011  0 ChCihninaa InInddiaia UU.S.S. .  Countries in  this work  Indonesia Lu et al., 2011; EPA  Annual ARP report 2013  If all projected plants become operational, Asian coal emissions of SO2 and  NOx could triple by 2030. Indonesia and Vietnam together account for 67%  of this projected increase, as well as an additional 35 million people by 2030.  Tg yr-1  GEOS-Chem simulates the concentrations of pollutants  Emission inventories  Pollutant co ncentrations     125000 40oN 40oN 30oN    10.00 100000 5.00 30oN     750002.00 20oN  20o1.00 N  50000  0.50   o o Assimilated meteorology 10 N  10 N 25000 0.20   0o  0.10 0o  0 10oS 10oS  100oE 120oE 140oE   100oE 120oE 140oE   Global 3-D CTM   

40oN 350000 30oN  300000 250000 GEOS-Chem is a global 3-D chemical transport mod20eoNl used by many 200000 research groups around the world to advance our undo erstanding of  150000 10 N 100000 atmospheric composition and to answer policy relevant questions pertaining 50000 0 to air quality and climate change. 0o 10oS  100oE 120oE 140oE   Project Objectives  1.  Calculate surface PM and ozone concentrations due to both present day  and estimated 2030 coal emissions in East and Southeast Asia  (excluding emissions from China and India).  

2.  Estimate the human health burden of this rising coal pollution.  Approach  1.  Attribute changes in PM and ozone concentrations due to both present day  (2011) and projected 2030 coal emissions using GEOS-Chem  

2.  Apply concentration-response relationships from the literature (Krewski et  al., 2009; Anenberg et al., 2010) to estimate the premature mortality due to  coal-related pollution.  

Coal pollution correlates with populated areas   ΔPM  2.5 from 2030 Coal  ΔOzone fro m 2030 C oal     125000 125000 40oN 40oN 40oN 40oN Hanoi  10.00  10.00  100000 10000030oN  53.00o0N 30oN  5.00 30oN      75000  750002.00 2.00 20oN  20oN 20o1.00 N  1.00  20oN50000  50000  0.50  0.50 10oN  10oN 10oN    

21500oN00 25000  0.20  0.20    0o  0.100o 0o  0.10  0 0o  0 µg m-3 ppb  10oS 10oS 10oS 10oS  100oE 120oE 140oE   100oE  10102o0EoE 12104o0EoE  140oE   100oE 120oE 140oE   Jakarta     Koplitz et al., in prep  

40oN The largest annual average enhanc40 o emN ents in PM from coal occur near  populated areas, particularly H ano3i 5a000ond0 Jakarta. Ozone enhancements30oN   are 35 0000 highest over Sumatra in Indonesia,33 0a00s0N0 w0 ell as much of Thailand and 300000250000 o Vietnam.   250000 20 N 22000o0N00 200000 150000 150000 10oN 11000o0N00 100000 50000 50000 o  0 0o  00 10oS 10oS  100oE 120oE 140oE   100oE 120oE 140oE   

  125000 40oN 40oN  10.00 10000030oN  5.00 30oN     750002.00 20oN  o1.00 20 N  50000  0.50   10oN 10oN 25000  0.20   0o  0.10 0o  0 Expo1s0ouS re depends on both pollution l1e0oSvels and population density     100 oE 120oE 140oE   100oE  120oE 140oE   30oN 30oN 30oN 2010 Popu lation Map ΔPM from 2030 Coal 25oN 2.5    25oN 25oN    10.00 125000  350000 40o3N50000 o  o 20oN 40 N 40 N 20oN 20oN  5.00  300000 300000 350000 10.00 100000 o o   15oN 15oN  250000 302N50000  30000150N 30oN   25.0.000 30oN    200000 200000 250000 

10oN  1.00  75000 10oN 10oN o  2.00  150000 201N50000 200000 20oN  o01.5.000 20 N  150000  500005oN 5oN 5oN  100000 100000 o 100000   00.2.050    50000  105N0000 o o50000 0o 10 N  10 N 250000o 0o  0.10 0  0 o   0.20  0 population0 5oS 0o  0.10 0o  05oS 5oS µg m-3   o o o   10oS 90 E 95 E 100 E 105 oE 110oE 115oE 120oE 125oE 130oE 90oE 95oE90oE 100oE95oE 105o1E00oE 110o1E05oE 115o1E10oE 120o1E15oE 125o1E20oE 130o1E25oE 130oE  100oE 120oE 140oE  10oS 10oS o  100oE  120oE 140oE   100oE 120oE 140oE  30 N  Total Exposure in 2030  (ΔPM2.5 x Population)   25oN    125000 40oN 40oN 40oN  100000 10.0020o N 100000o 3500003T0oN tal exposure is highest in  5.00 30 oN   30oN     15oN  7 375050000000000 In 2.00 25000020oNdonesia and Vietnam, foll owed o1.00 2020NoN  525000000000 by China due to high popula tion 10oN   0.50 150000 1l0 o o eNvels in southern China ne ar 1010NoN  21500000000 0.20 5oN  25000  50000V0ioetnamese emissions. 0.10 0o 00o  00o  0 10oS 10oSo  100oE 120oE 140oE  5oS 10 S 100o 100EoE 1210 o 20EoE 11440 o 0EoE     9 0oE 95oE 100oE 105oE 110oE 115oE 120oE 125oE 130oE 

40oN 350000 30oN  300000 250000 20oN 200000 150000 10oN 100000 50000 0o  0 10oS  100oE 120oE 140oE   Project Objectives  1.  Calculate surface PM and ozone concentrations due to both present day  and estimated 2030 coal emissions in East and Southeast Asia  (excluding emissions from China and India).  

2.  Estimate the human health burden of this rising coal pollution.  Approach  1.  Attribute changes in PM and ozone concentrations due to both present day  (2011) and projected 2030 coal emissions using GEOS-Chem  

2.  Apply concentration-response relationships from the literature (Krewski et  al., 2009; Anenberg et al., 2010) to estimate the premature mortality due to  coal-related pollution.  

We estimate 16,000 deaths annually from current coal  Excess Deaths Per Year  2011:  14,860 PM  1,530 ozone Including a 10% population  16,390 total increase by 2030 in both   Indonesia and Vietnam, we  2030 increase:  estimate 43,000 deaths  24,160 PM annually by 2030 if all  2,390 ozone projected plants become  26,550 total operational.  

= 42,940  excess deaths  per year  Assessment of national contributions to coal pollution is ongoing  Annual Mortality from Vietnam Coal  Contribution of V ietnam to 2030 ΔPM2.5	   	      30oN 30oN 2011: 125000 10.00 3,827 25oN  25oN 100000 5.00    20oN  20oN2.00 2030 increase:   75000 15oN  1.00 15oN 14,169  50000 0.50     10oN 10oN 25000  0.20 = 17,996   5oN  0.10 5oN excess  0 µg m-3 deaths  0o 0o 90oE 100oE 110oE 120oE 130oE 90oE 100oE 11p0oeEr ye1a2r0o E 130oE 



30oN 25oN We have assessed country leve l co35n0t0r0i0butions for South Korea, Vietnam,  o and Taiwan. Results for Japan, Ind3o0n00e0s0 ia, Malaysia, Thailand, Myanmar 20 N 250000 and the Philippines will be complet2e0d00 0o0ver the next few months.  15oN 150000 10oN 100000 50000 5oN  0 0o 90oE 100oE 110oE 120oE 130oE New projections for Japanese plants could change results  Bloomberg Business, April 09 2015  17	   in	   our	   analysis	    Including emissions from recently announced coal plants could change  estimates of health effects from Japanese coal emissions.  Summary  •  Coal fired power plant emissions of SO2 and NOx form  particulate matter and ozone which are detrimental to human  health.  •  Coal emissions in Southeast Asia are projected to triple by 2030.  •  Without abatement, these projected emissions could lead to  more than 40,000 excess deaths every year.  





Please email skoplitz@fas.harvard.edu for more information about this  work. Thank you for listening!  
